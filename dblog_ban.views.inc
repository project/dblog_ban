<?php

/**
 * @file
 * Provide views data for dblog_ban.module.
 */

/**
 * Implements hook_views_data().
 */
function dblog_ban_views_data() {
  $data = [];

  // Provides a field handler that displays a ban/unban link for a watchdog IP.
  $data['watchdog']['dblog_ban_ban_unban_link'] = [
    'title' => t('Ban/Unban link'),
    'help' => t('Show a link to ban or unban the hostname (IP address) associated with a log message.'),
    'field' => [
      'id' => 'dblog_ban_ban_unban_link',
    ],
  ];

  return $data;
}
