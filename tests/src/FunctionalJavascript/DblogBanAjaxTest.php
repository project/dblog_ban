<?php

namespace Drupal\Tests\dblog_ban\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\dblog_ban\Traits\DblogBanTestTrait;
use Drupal\Tests\dblog_ban\Traits\RandomIpV4AddressGenerator;

/**
 * Test that we can use the AJAX ban/unban links.
 *
 * @group dblog_ban
 */
class DblogBanAjaxTest extends WebDriverTestBase {
  use DblogBanTestTrait;
  use RandomIpV4AddressGenerator;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['dblog_ban', 'dblog_ban_test'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $user = $this->drupalCreateUser(['ban IP addresses', 'access site reports']);
    $this->drupalLogin($user);
  }

  /**
   * Test using AJAX links to ban and unban an IP address from its log message.
   *
   * In this test, the IP starts unbanned, gets banned, and gets unbanned again.
   */
  public function testAjaxBanAndUnban(): void {
    $this->setUseAjaxLinksSetting(TRUE);

    // Generate a random public (i.e.: bannable) IP address, create a random log
    // message for it, and assert that it starts off unbanned.
    $offendingIp = $this->getRandomPublicIpV4();
    $this->addRandomLogMessageFromIp($offendingIp);
    $this->assertIpIsNotBanned($offendingIp);

    // Load our fork of the admin/reports/dblog page with the ban/unban field
    // already added; and assert it looks correct.
    $this->drupalGet('admin/reports/dblog_ban');
    $this->assertSession()->pageTextContains('Recent log messages');

    [$banLink, $unbanLink] = $this->getBanUnbanLinkSelectors($offendingIp);
    $page = $this->getSession()->getPage();

    // 1: Ban the IP, wait for an Unban link to appear, check IP is now banned.
    $page->find('css', $banLink)->click();
    $this->assertSession()->waitForElementVisible('css', $unbanLink);
    $this->assertIpIsBanned($offendingIp);

    // 2: Unban the IP, wait for a Ban link to appear, check IP is now unbanned.
    $page->find('css', $unbanLink)->click();
    $this->assertSession()->waitForElementVisible('css', $banLink);
    $this->assertIpIsNotBanned($offendingIp);
  }

  /**
   * Test using AJAX links to unban and ban an IP address from its log message.
   *
   * In this test, the IP starts banned, gets unbanned, and gets banned again.
   */
  public function testAjaxUnbanAndBan(): void {
    $this->setUseAjaxLinksSetting(TRUE);

    // Generate a random public (i.e.: bannable) IP address, create a random log
    // message for it, ban it, and assert that it starts off banned.
    $offendingIp = $this->getRandomPublicIpV4();
    $this->addRandomLogMessageFromIp($offendingIp);
    $this->container->get('ban.ip_manager')->banIp($offendingIp);
    $this->assertIpIsBanned($offendingIp);

    // Load our fork of the admin/reports/dblog page with the ban/unban field
    // already added; and assert it looks correct.
    $this->drupalGet('admin/reports/dblog_ban');
    $this->assertSession()->pageTextContains('Recent log messages');

    [$banLink, $unbanLink] = $this->getBanUnbanLinkSelectors($offendingIp);
    $page = $this->getSession()->getPage();

    // 1: Unban the IP, wait for a Ban link to appear, check IP is now unbanned.
    $page->find('css', $unbanLink)->click();
    $this->assertSession()->waitForElementVisible('css', $banLink);
    $this->assertIpIsNotBanned($offendingIp);

    // 2: Ban the IP, wait for an Unban link to appear, check IP is now banned.
    $page->find('css', $banLink)->click();
    $this->assertSession()->waitForElementVisible('css', $unbanLink);
    $this->assertIpIsBanned($offendingIp);
  }

}
