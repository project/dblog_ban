<?php

namespace Drupal\Tests\dblog_ban\Kernel;

use Drupal\Core\Database\Database;
use Drupal\Tests\migrate_drupal\Kernel\d7\MigrateDrupal7TestBase;

/**
 * Test the dblog_ban_settings migration.
 *
 * @group dblog_ban
 */
class MigrateD7SettingsTest extends MigrateDrupal7TestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['dblog_ban'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->setUpD7EnableExtension('module', 'dblog_ban', 7000, 0);
  }

  /**
   * Test the dblog_ban_settings migration.
   *
   * @throws \Exception
   *   Throws an \Exception if the database cannot be modified.
   */
  public function testMigration(): void {
    // Try migrating when use_ajax_links is FALSE.
    $this->setD7Variable('dblog_ban_use_ajax_links', FALSE);
    $this->executeMigrations(['dblog_ban_settings']);
    $this->assertFalse($this->config('dblog_ban.settings')->get('use_ajax_links'));

    $this->resetMigration('dblog_ban_settings');

    // Try migrating when use_ajax_links is TRUE.
    $this->setD7Variable('dblog_ban_use_ajax_links', TRUE);
    $this->executeMigrations(['dblog_ban_settings']);
    $this->assertTrue($this->config('dblog_ban.settings')->get('use_ajax_links'));
  }

  /**
   * Prepare the migration under test to be re-run.
   *
   * @param string $migrationId
   *   The migration to be reset.
   */
  protected function resetMigration(string $migrationId): void {
    $migration = $this->getMigration($migrationId);
    $migration->getIdMap()->prepareUpdate();
  }

  /**
   * Set a variable in the D7 database.
   *
   * @param string $key
   *   The name of the variable to set.
   * @param mixed $value
   *   The value of the variable. Note it will be \serialize()ed.
   *
   * @throws \Exception
   *   Throws an \Exception if the database cannot be modified. Leave uncaught
   *   to allow the test case to fail.
   */
  protected function setD7Variable(string $key, $value): void {
    Database::getConnection('default', 'migrate')
      ->upsert('variable')
      ->key('name')
      ->fields(['name', 'value'])
      ->values([
        'name' => $key,
        'value' => \serialize($value),
      ])
      ->execute();
  }

  /**
   * Simulate enabling an extension in the D7 database.
   *
   * @param string $type
   *   The type of extension to enable.
   * @param string $extensionName
   *   The name of the extension to enable.
   * @param int $schemaVersion
   *   The version of the schema.
   * @param int $weight
   *   The weight of the extension.
   */
  protected function setUpD7EnableExtension(string $type, string $extensionName, int $schemaVersion, int $weight): void {
    Database::getConnection('default', 'migrate')
      ->insert('system')
      ->fields([
        'filename',
        'name',
        'type',
        'owner',
        'status',
        'bootstrap',
        'schema_version',
        'weight',
      ])
      ->values([
        'filename' => sprintf('sites/all/modules/%s/%s.module', $extensionName, $extensionName),
        'name' => $extensionName,
        'type' => $type,
        'owner' => '',
        'status' => 1,
        'bootstrap' => 0,
        'schema_version' => $schemaVersion,
        'weight' => $weight,
      ])
      ->execute();
  }

}
