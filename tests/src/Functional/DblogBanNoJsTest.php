<?php

namespace Drupal\Tests\dblog_ban\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\dblog_ban\Traits\DblogBanTestTrait;
use Drupal\Tests\dblog_ban\Traits\RandomIpV4AddressGenerator;

/**
 * Test that we can use the NoJS ban/unban links.
 *
 * @group dblog_ban
 */
class DblogBanNoJsTest extends BrowserTestBase {
  use DblogBanTestTrait;
  use RandomIpV4AddressGenerator;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['dblog_ban', 'dblog_ban_test'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $user = $this->drupalCreateUser(['ban IP addresses', 'access site reports']);
    $this->drupalLogin($user);
  }

  /**
   * Test using the confirm form to ban an IP address from its log message.
   */
  public function testNoJsBan(): void {
    // Generate a random public (i.e.: bannable) IP address, create a random log
    // message for it, and assert that it starts off unbanned.
    $offendingIp = $this->getRandomPublicIpV4();
    $this->addRandomLogMessageFromIp($offendingIp);
    $this->assertIpIsNotBanned($offendingIp);

    // Load our fork of the admin/reports/dblog page with the ban/unban field
    // already added; and assert it looks correct.
    $this->drupalGet('admin/reports/dblog_ban');
    $this->assertSession()->pageTextContains('Recent log messages');

    [$banLink] = $this->getBanUnbanLinkSelectors($offendingIp);

    // Click the link to ban the IP and assert that the confirm form looks
    // correct.
    $this->click($banLink);
    $this->assertSession()->pageTextContains("Are you sure you want to ban $offendingIp?");
    $this->assertSession()->pageTextContains('This action cannot be undone.');
    $this->assertSession()->elementExists('css', 'input[type="submit"][value="Confirm"]');
    $this->assertSession()->elementExists('css', 'a.button[data-drupal-selector="edit-cancel"]');

    // Click confirm; check IP is now banned.
    $this->submitForm([], 'Confirm');
    $this->assertIpIsBanned($offendingIp);
  }

  /**
   * Test using the confirm form to unban an IP address from its log message.
   */
  public function testNoJsUnban(): void {
    // Generate a random public (i.e.: bannable) IP address, create a random log
    // message for it, ban it, and assert that it starts off banned.
    $offendingIp = $this->getRandomPublicIpV4();
    $this->addRandomLogMessageFromIp($offendingIp);
    $this->container->get('ban.ip_manager')->banIp($offendingIp);
    $this->assertIpIsBanned($offendingIp);

    // Load our fork of the admin/reports/dblog page with the ban/unban field
    // already added; and assert it looks correct.
    $this->drupalGet('admin/reports/dblog_ban');
    $this->assertSession()->pageTextContains('Recent log messages');

    [, $unbanLink] = $this->getBanUnbanLinkSelectors($offendingIp);

    // Click the link to unban the IP and assert that the confirm form looks
    // correct.
    $this->click($unbanLink);
    $this->assertSession()->pageTextContains("Are you sure you want to unban $offendingIp?");
    $this->assertSession()->pageTextContains('This action cannot be undone.');
    $this->assertSession()->elementExists('css', 'input[type="submit"][value="Confirm"]');
    $this->assertSession()->elementExists('css', 'a.button[data-drupal-selector="edit-cancel"]');

    // Click confirm; check IP is now unbanned.
    $this->submitForm([], 'Confirm');
    $this->assertIpIsNotBanned($offendingIp);
  }

}
