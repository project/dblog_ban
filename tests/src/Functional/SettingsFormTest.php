<?php

namespace Drupal\Tests\dblog_ban\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Test the dblog_ban settings form.
 *
 * @group dblog_ban
 */
class SettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['dblog_ban'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test the dblog_ban settings form.
   */
  public function testSettingsForm(): void {
    $this->drupalLogin($this->drupalCreateUser(['change global dblog_ban settings']));

    // Assert that the controls are on the settings form.
    $this->drupalGet(Url::fromRoute('dblog_ban.settings'));
    $this->assertSession()->pageTextContains('Use AJAX ban/unban links');
    $this->assertSession()->pageTextContains('Using AJAX links makes the ban/unban operation take effect immediately, without confirmation. This can be useful to prevent you from losing your place in the list; but in rare cases when your computer has multiple public-facing IPs, it becomes slightly easier to ban yourself.');

    // Assert that we can disable use_ajax_links and that gets saved to config.
    $this->drupalGet(Url::fromRoute('dblog_ban.settings'));
    $edit = ['use_ajax_links' => 0];
    $this->submitForm($edit, 'Save configuration');
    $this->assertFalse($this->config('dblog_ban.settings')->get('use_ajax_links'));

    // Assert that we can enable use_ajax_links and that gets saved to config.
    $this->drupalGet(Url::fromRoute('dblog_ban.settings'));
    $edit = ['use_ajax_links' => 1];
    $this->submitForm($edit, 'Save configuration');
    $this->assertTrue($this->config('dblog_ban.settings')->get('use_ajax_links'));
  }

}
