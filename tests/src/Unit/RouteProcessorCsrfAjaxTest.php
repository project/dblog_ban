<?php

namespace Drupal\Tests\dblog_ban\Unit;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Tests\UnitTestCase;
use Drupal\dblog_ban\Services\RouteProcessorCsrfAjax;
use Symfony\Component\Routing\Route;

/**
 * @coversDefaultClass \Drupal\dblog_ban\Services\RouteProcessorCsrfAjax
 * @group Access
 * @todo Remove this class when https://www.drupal.org/node/2670798 is merged.
 * @internal
 */
class RouteProcessorCsrfAjaxTest extends UnitTestCase {

  /**
   * The mock CSRF token generator.
   *
   * @var \Drupal\Core\Access\CsrfTokenGenerator|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $csrfToken;

  /**
   * The route processor.
   *
   * @var \Drupal\dblog_ban\Services\RouteProcessorCsrfAjax
   */
  protected $processor;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->csrfToken = $this->createMock('Drupal\Core\Access\CsrfTokenGenerator');

    $this->processor = new RouteProcessorCsrfAjax($this->csrfToken);
  }

  /**
   * Tests the processOutbound() method with CSRF token for ajax/nojs routes.
   */
  public function testProcessOutboundTokenAjax() {
    $route = new Route('/test-path/{js}', [], ['_dblog_ban_csrf_ajax_token' => 'TRUE']);
    $parameters = ['js' => 'nojs'];

    $bubbleable_metadata = new BubbleableMetadata();
    $this->processor->processOutbound('test', $route, $parameters, $bubbleable_metadata);
    // 'token' should be added to the parameters array.
    $this->assertArrayHasKey('token', $parameters);
    // Bubbleable metadata of routes with a _dblog_ban_csrf_ajax_token route
    // requirement is a placeholder.
    $path = 'test-path/{js}';
    $placeholder = Crypt::hashBase64($path);
    $placeholder_render_array = [
      '#lazy_builder' => [
        'dblog_ban_route_processor_csrf_ajax:renderPlaceholderCsrfToken',
        [$path],
      ],
    ];
    $this->assertSame($parameters['token'], $placeholder);
    $this->assertEquals((new BubbleableMetadata())->setAttachments(['placeholders' => [$placeholder => $placeholder_render_array]]), $bubbleable_metadata);
  }

}
