<?php

namespace Drupal\Tests\dblog_ban\Traits;

/**
 * Functions to randomly generate IPv4 addresses.
 */
trait RandomIpV4AddressGenerator {

  /**
   * Get a random IPv4 address on a private network.
   *
   * The tool https://cidr.xyz is handy for interpreting CIDR address ranges.
   *
   * @return string
   *   A random IPv4 address in one of the following CIDR address ranges that
   *   are reserved for private networks:
   *   - 10.0.0.0/8
   *   - 172.16.0.0/12
   *   - 192.168.0.0/16
   */
  protected function getRandomPrivateIpV4(): string {
    // Randomly choose which range we want to generate an IP address in.
    $range = \random_int(0, 2);
    switch ($range) {
      // CIDR 10.0.0.0/8.
      case 0:
        return '10.' . \random_int(0, 255)
          . '.' . \random_int(0, 255)
          . '.' . \random_int(0, 255);

      // CIDR 172.16.0.0/12.
      case 1:
        return '172.' . \random_int(16, 31)
          . '.' . \random_int(0, 255)
          . '.' . \random_int(0, 255);

      // CIDR 192.168.0.0/16.
      default:
        return '192.168.' . \random_int(0, 255)
          . '.' . \random_int(0, 255);
    }
  }

  /**
   * Get a random IPv4 address outside on a public network.
   *
   * The tool https://cidr.xyz is handy for interpreting CIDR address ranges.
   *
   * @return string
   *   A random IPv4 address in one of the following CIDR address ranges that
   *   are NOT reserved for private or loopback networks:
   *   - 0.0.0.0 - 9.255.255.255
   *   - 11.0.0.0 - 126.255.255.255
   *   - 128.0.0.0 - 171.255.255.255
   *   - 172.0.0.0 - 172.15.255.255
   *   - 172.32.0.0 - 172.255.255.255
   *   - 173.0.0.0 - 191.255.255.255
   *   - 192.0.0.0 - 192.167.255.255
   *   - 192.169.0.0 - 192.255.255.255
   *   - 193.0.0.0 - 255.255.255.255
   */
  protected function getRandomPublicIpV4(): string {
    // Randomly choose which range we want to generate an IP address in.
    $region = \random_int(0, 8);
    switch ($region) {
      case 0:
        return \random_int(0, 9)
          . '.' . \random_int(0, 255)
          . '.' . \random_int(0, 255)
          . '.' . \random_int(0, 255);

      // Skip private range 10.0.0.0/8.
      case 1:
        return \random_int(11, 126)
          . '.' . \random_int(0, 255)
          . '.' . \random_int(0, 255)
          . '.' . \random_int(0, 255);

      // Skip loopback range 127.0.0.0/8.
      case 2:
        return \random_int(128, 171)
          . '.' . \random_int(0, 255)
          . '.' . \random_int(0, 255)
          . '.' . \random_int(0, 255);

      case 3:
        return '172.' . \random_int(0, 15)
          . '.' . \random_int(0, 255)
          . '.' . \random_int(0, 255);

      // Skip private range 172.16.0.0/12.
      case 4:
        return '172.' . \random_int(32, 255)
          . '.' . \random_int(0, 255)
          . '.' . \random_int(0, 255);

      case 5:
        return \random_int(173, 191)
          . '.' . \random_int(0, 255)
          . '.' . \random_int(0, 255)
          . '.' . \random_int(0, 255);

      case 6:
        return '192.' . \random_int(0, 167)
          . '.' . \random_int(0, 255)
          . '.' . \random_int(0, 255);

      // Skip private range 192.168.0.0/16.
      case 7:
        return '192.' . \random_int(169, 255)
          . '.' . \random_int(0, 255)
          . '.' . \random_int(0, 255);

      // Skip reserved range 240.0.0.0/4 at the end.
      default:
        return \random_int(193, 239)
          . '.' . \random_int(0, 255)
          . '.' . \random_int(0, 255)
          . '.' . \random_int(0, 255);
    }
  }

}
