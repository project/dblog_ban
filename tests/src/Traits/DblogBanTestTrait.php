<?php

namespace Drupal\Tests\dblog_ban\Traits;

/**
 * Functions useful in dblog_ban tests.
 */
trait DblogBanTestTrait {

  /**
   * Log a random message from a given IP address.
   *
   * @param string $ip
   *   The IP address to log a message from.
   *
   * @see \Drupal\dblog\Logger\DbLog::log()
   */
  protected function addRandomLogMessageFromIp(string $ip): void {
    $level = \random_int(0, 7);
    $message = $this->getRandomGenerator()->string();
    $context = [
      'ip' => $ip,
      'uid' => \random_int(0, 10),
      'channel' => $this->getRandomGenerator()->string(63),
      'link' => $this->getRandomGenerator()->string(),
      'request_uri' => $this->getRandomGenerator()->string(),
      'referer' => $this->getRandomGenerator()->string(),
      // Use the current timestamp so our test log message appears at the top of
      // the list, i.e.: so it doesn't accidentally get buried on another page.
      'timestamp' => time(),
    ];
    $this->container->get('logger.dblog')->log($level, $message, $context);
  }

  /**
   * Assert that a given IP address is banned.
   *
   * @param string $ip
   *   The IP address to check.
   */
  protected function assertIpIsBanned(string $ip): void {
    $this->assertTrue($this->container->get('ban.ip_manager')->isBanned($ip));
  }

  /**
   * Assert that a given IP address is not banned (has been unbanned).
   *
   * @param string $ip
   *   The IP address to check.
   */
  protected function assertIpIsNotBanned(string $ip): void {
    $this->assertFalse($this->container->get('ban.ip_manager')->isBanned($ip));
  }

  /**
   * Get selectors for both the AJAX ban and unban links.
   *
   * @param string $ip
   *   The IP address to get the ban/unban selectors for.
   *
   * @return string[]
   *   The first element in the array is the ban link; the second is an unban
   *   link. Suitable for use with list($ban, $unban).
   */
  protected function getBanUnbanLinkSelectors(string $ip): array {
    $base = sprintf('[data-dblog_ban-ip="%s"]', $ip);
    return [
      $base . "[data-dblog_ban-action='ban']",
      $base . "[data-dblog_ban-action='unban']",
    ];
  }

  /**
   * Ensure that the config dblog_ban.use_ajax_links is a certain state.
   *
   * @param bool $setting
   *   The setting that dblog_ban.use_ajax_links should be in.
   */
  protected function setUseAjaxLinksSetting(bool $setting): void {
    $this->container->get('config.factory')->getEditable('dblog_ban')
      ->set('use_ajax_links', $setting);
  }

}
