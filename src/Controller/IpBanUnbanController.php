<?php

namespace Drupal\dblog_ban\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\ban\BanIpManagerInterface;
use Drupal\dblog_ban\Form\IpBanConfirmForm;
use Drupal\dblog_ban\Form\IpUnbanConfirmForm;
use Drupal\dblog_ban\Services\BanLinkGenerator;
use Drupal\dblog_ban\Services\IpValidator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Respond to routing requests to ban and unban IP addresses.
 */
class IpBanUnbanController extends ControllerBase {

  /**
   * A ban IP manager.
   *
   * @var \Drupal\ban\BanIpManagerInterface
   */
  private $banIpManager;

  /**
   * Service to generate ban and unban links.
   *
   * @var \Drupal\dblog_ban\Services\BanLinkGenerator
   */
  private $banLinkGenerator;

  /**
   * Service to validate hostnames or IP addresses.
   *
   * @var \Drupal\dblog_ban\Services\IpValidator
   */
  private $ipValidator;

  /**
   * Constructs a IpBanUnbanAjaxController object.
   *
   * @param \Drupal\ban\BanIpManagerInterface $banIpManager
   *   A ban IP manager.
   * @param \Drupal\dblog_ban\Services\BanLinkGenerator $banLinkGenerator
   *   Service to generate ban and unban links.
   * @param \Drupal\dblog_ban\Services\IpValidator $ipValidator
   *   Service to validate hostnames or IP addresses.
   */
  public function __construct(BanIpManagerInterface $banIpManager, BanLinkGenerator $banLinkGenerator, IpValidator $ipValidator) {
    $this->banIpManager = $banIpManager;
    $this->banLinkGenerator = $banLinkGenerator;
    $this->ipValidator = $ipValidator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('ban.ip_manager'),
      $container->get('dblog_ban.link_generator'),
      $container->get('dblog_ban.ip_validator')
    );
  }

  /**
   * Respond to an AJAX request to ban an IP address.
   *
   * @param string $js
   *   Either 'ajax' or 'nojs'. If 'ajax', then this will immediately ban the IP
   *   address and return an AJAX response to replace the link. Otherwise, this
   *   will return a "ban" confirmation form.
   * @param string $ip
   *   The IP address to ban.
   *
   * @return array|\Drupal\Core\Ajax\AjaxResponse
   *   If $js is 'ajax', then an AjaxResponse to replace the ban link with an
   *   unban link. Otherwise, this will return a form array.
   */
  public function ajaxBan(string $js, string $ip) {
    if ($js !== 'ajax') {
      return $this->formBuilder()
        ->getForm(IpBanConfirmForm::class, $ip);
    }

    $response = new AjaxResponse();
    $rawIp = \urldecode($ip);

    if ($this->ipValidator->isValidIp($rawIp) && !$this->ipValidator->isMyIp($rawIp)) {
      $this->banIpManager->banIp($rawIp);
      $replacementLink = $this->banLinkGenerator->getUnbanLink($rawIp);
      $response->addCommand(new ReplaceCommand("[data-dblog_ban-ip='$rawIp'][data-dblog_ban-action='ban']", $replacementLink));
    }
    return $response;
  }

  /**
   * Respond to an AJAX request to unban an IP address.
   *
   * @param string $js
   *   Either 'ajax' or 'nojs'. If 'ajax', then this will immediately unban the
   *   IP address and return an AJAX response to replace the link. Otherwise,
   *   this will return an "unban" confirmation form.
   * @param string $ip
   *   The IP address to unban.
   *
   * @return array|\Drupal\Core\Ajax\AjaxResponse
   *   If $js is 'ajax', then an AjaxResponse to replace the unban link with a
   *   ban link. Otherwise, this will return a form array.
   */
  public function ajaxUnban(string $js, string $ip) {
    if ($js !== 'ajax') {
      return $this->formBuilder()
        ->getForm(IpUnbanConfirmForm::class, $ip);
    }

    $response = new AjaxResponse();
    $rawIp = \urldecode($ip);

    if ($this->ipValidator->isValidIp($rawIp)) {
      $this->banIpManager->unbanIp($rawIp);
      $replacementLink = $this->banLinkGenerator->getBanLink($rawIp);
      $response->addCommand(new ReplaceCommand("[data-dblog_ban-ip='$rawIp'][data-dblog_ban-action='unban']",
        $replacementLink));
    }
    return $response;
  }

}
