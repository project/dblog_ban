<?php

namespace Drupal\dblog_ban\Plugin\views\field;

use Drupal\ban\BanIpManagerInterface;
use Drupal\dblog_ban\Services\BanLinkGenerator;
use Drupal\dblog_ban\Services\IpValidator;
use Drupal\dblog_ban\Services\WatchdogViewsRowParser;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a field handler that displays a ban/unban link for a watchdog IP.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("dblog_ban_ban_unban_link")
 */
class DblogBanBanUnbanLink extends FieldPluginBase {

  /**
   * A ban IP manager.
   *
   * @var \Drupal\ban\BanIpManagerInterface
   */
  protected $banIpManager;

  /**
   * Service to generate ban and unban links.
   *
   * @var \Drupal\dblog_ban\Services\BanLinkGenerator
   */
  private $banLinkGenerator;

  /**
   * Service to validate hostnames or IP addresses.
   *
   * @var \Drupal\dblog_ban\Services\IpValidator
   */
  private $ipValidator;

  /**
   * Service to get a hostname (IP address) from a Views row of type 'watchdog'.
   *
   * @var \Drupal\dblog_ban\Services\WatchdogViewsRowParser
   */
  private $watchdogViewsRowParser;

  /**
   * Constructs a DblogBanLink object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\ban\BanIpManagerInterface $banIpManager
   *   A ban IP manager.
   * @param \Drupal\dblog_ban\Services\BanLinkGenerator $banLinkGenerator
   *   Service to generate ban and unban links.
   * @param \Drupal\dblog_ban\Services\IpValidator $ipValidator
   *   Service to validate hostnames or IP addresses.
   * @param \Drupal\dblog_ban\Services\WatchdogViewsRowParser $watchdogViewsRowParser
   *   Service to get a hostname (IP address) from a Views row of type
   *   'watchdog'.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, BanIpManagerInterface $banIpManager, BanLinkGenerator $banLinkGenerator, IpValidator $ipValidator, WatchdogViewsRowParser $watchdogViewsRowParser) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->banIpManager = $banIpManager;
    $this->banLinkGenerator = $banLinkGenerator;
    $this->ipValidator = $ipValidator;
    $this->watchdogViewsRowParser = $watchdogViewsRowParser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self($configuration, $plugin_id, $plugin_definition,
      $container->get('ban.ip_manager'),
      $container->get('dblog_ban.link_generator'),
      $container->get('dblog_ban.ip_validator'),
      $container->get('dblog_ban.watchdog_views_row_parser')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function clickSortable() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $ip = $this->watchdogViewsRowParser->getHostnameFromWatchdogResultRow($values);
    // If the IP we get back is our own IP, then return without making a link.
    if ($this->ipValidator->isMyIp($ip)) {
      return [];
    }

    // If the IP we get back is invalid, then return without making a link.
    if (!$this->ipValidator->isValidIp($ip)) {
      return [];
    }

    if ($this->banIpManager->isBanned($ip)) {
      $value = $this->banLinkGenerator->getUnbanLink($ip);
    }
    else {
      $value = $this->banLinkGenerator->getBanLink($ip);
    }
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // No-op.
  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

}
