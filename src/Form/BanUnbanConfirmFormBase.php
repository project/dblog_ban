<?php

namespace Drupal\dblog_ban\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\ban\BanIpManagerInterface;
use Drupal\dblog_ban\Services\IpValidator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * A base for ban and unban ConfirmForms, which stores a banIp upon form build.
 */
abstract class BanUnbanConfirmFormBase extends ConfirmFormBase {

  /**
   * The IP address to ban or unban.
   *
   * @var string
   */
  protected $banIp;

  /**
   * A ban IP manager.
   *
   * @var \Drupal\ban\BanIpManagerInterface
   */
  protected $banIpManager;

  /**
   * Service to validate hostnames or IP addresses.
   *
   * @var \Drupal\dblog_ban\Services\IpValidator
   */
  protected $ipValidator;

  /**
   * Constructs a BanUnbanConfirmFormBase object.
   *
   * @param \Drupal\ban\BanIpManagerInterface $banIpManager
   *   A ban IP manager.
   * @param \Drupal\dblog_ban\Services\IpValidator $ipValidator
   *   Service to validate hostnames or IP addresses.
   */
  public function __construct(BanIpManagerInterface $banIpManager, IpValidator $ipValidator) {
    $this->banIpManager = $banIpManager;
    $this->ipValidator = $ipValidator;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('dblog.overview');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $ip = '') {
    // If the IP is invalid, return a 404 Not Found.
    if ($this->ipValidator->isValidIp($ip) === FALSE) {
      throw new NotFoundHttpException();
    }

    // If we get here, set the property, and build the form.
    $this->banIp = $ip;
    return parent::buildForm($form, $form_state);
  }

}
