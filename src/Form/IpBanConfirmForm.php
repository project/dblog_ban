<?php

namespace Drupal\dblog_ban\Form;

use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Confirmation form to let an administrator ban an IP address.
 */
class IpBanConfirmForm extends BanUnbanConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('ban.ip_manager'),
      $container->get('dblog_ban.ip_validator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dblog_ban_confirm_ban';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to ban %ip?', [
      '%ip' => $this->banIp,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $ip = $this->banIp;

    if ($this->ipValidator->isValidIp($ip) && !$this->ipValidator->isMyIp($ip)) {
      $this->banIpManager->banIp($ip);
      $this->messenger()->addStatus($this->t('The IP address %ip has been banned.', [
        '%ip' => $ip,
      ]));
    }
    else {
      $this->messenger()->addError('Unable to ban the ip address %ip because it is not valid, or it is your current IP address.', [
        '%ip' => $ip,
      ]);
    }
  }

}
