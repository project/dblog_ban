<?php

namespace Drupal\dblog_ban\Form;

use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Confirmation form to let an administrator unban an IP address.
 */
class IpUnbanConfirmForm extends BanUnbanConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('ban.ip_manager'),
      $container->get('dblog_ban.ip_validator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dblog_ban_confirm_unban';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to unban %ip?', [
      '%ip' => $this->banIp,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $ip = $this->banIp;
    if ($this->ipValidator->isValidIp($ip)) {
      $this->banIpManager->unbanIp($ip);
      $this->messenger()->addStatus($this->t('The IP address %ip has been unbanned.', [
        '%ip' => $ip,
      ]));
    }
    else {
      $this->messenger()->addError($this->t('Unable to unban the ip address %ip because it is not valid.', [
        '%ip' => $ip,
      ]));
    }
  }

}
