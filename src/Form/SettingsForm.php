<?php

namespace Drupal\dblog_ban\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * A settings pane to allow administrators to change settings for this module.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['dblog_ban.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dblog_ban_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Use AJAX links.
    $form['use_ajax_links'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use AJAX ban/unban links'),
      '#description' => $this->t('Using AJAX links makes the ban/unban operation take effect immediately, without confirmation. This can be useful to prevent you from losing your place in the list; but in rare cases when your computer has multiple public-facing IPs, it becomes slightly easier to ban yourself.'),
      '#default_value' => $this->config('dblog_ban.settings')->get('use_ajax_links'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('dblog_ban.settings')
      ->set('use_ajax_links', $form_state->getValue('use_ajax_links'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
