<?php

namespace Drupal\dblog_ban\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RedirectDestinationTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Service to generate ban and unban links.
 */
class BanLinkGenerator implements ContainerInjectionInterface {
  use RedirectDestinationTrait;
  use StringTranslationTrait;

  /**
   * A configuration object factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * Constructs a BanLinkGenerator.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   A configuration object factory.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self($container->get('config.factory'));
  }

  /**
   * Generate a ban link for an IP address.
   *
   * Note this function will happily generate a ban link for any IP address,
   * including the current user. If the current user bans themselves, they will
   * not be able to access the unban link anymore, and will have to use Drush or
   * direct database access to unban themselves. We recommend that you only call
   * this function if
   * \Drupal\dblog_ban\Services\IpValidator::isMyIp() === FALSE.
   *
   * @param string $ip
   *   The IP address to generate the link for.
   *
   * @return array
   *   A render array for a ban link.
   */
  public function getBanLink(string $ip): array {
    $url = $this->getBanUrl($ip);

    $value = Link::fromTextAndUrl($this->t('Ban @host', [
      '@host' => $ip,
    ]), $url)->toRenderable();
    if ($this->areAjaxLinksEnabled()) {
      $value['#attached']['library'][] = 'core/drupal.ajax';
    }

    // @todo CSRF tokens are based on the path, user session, and a private key.
    //   The default cacheability metadata doesn't seem to work, so we're
    //   setting this to be uncacheable; but there is probably a better fix.
    $value['#cache']['max-age'] = 0;

    return $value;
  }

  /**
   * Generate a ban URL for an IP address.
   *
   * Note this function will happily generate a ban URL for any IP address,
   * including the current user. If the current user bans themselves, they will
   * not be able to access the unban link anymore, and will have to use Drush or
   * direct database access to unban themselves. We recommend that you only call
   * this function if
   * \Drupal\dblog_ban\Services\IpValidator::isMyIp() === FALSE.
   *
   * @param string $ip
   *   The IP address to generate the URL for.
   *
   * @return \Drupal\Core\Url
   *   A URL for a ban link.
   */
  public function getBanUrl(string $ip): Url {
    $urlIp = \urlencode($ip);
    $urlOptions = $this->getUrlOptions($ip, 'ban');

    return Url::fromRoute('dblog_ban.ban', [
      'js' => 'nojs',
      'ip' => $urlIp,
    ], $urlOptions);
  }

  /**
   * Generate an unban link for an IP address.
   *
   * While it is recommended to check whether
   * \Drupal\dblog_ban\Services\IpValidator::isMyIp() === FALSE before
   * displaying a ban link, the consequences of the current user unbanning
   * themselves is less serious: you don't necessarily need to check that before
   * calling this function.
   *
   * @param string $ip
   *   The IP address to generate the unban link for.
   *
   * @return array
   *   A render array for an unban link.
   */
  public function getUnbanLink(string $ip): array {
    $url = $this->getUnbanUrl($ip);

    $value = Link::fromTextAndUrl($this->t('Unban @host', [
      '@host' => $ip,
    ]), $url)->toRenderable();
    if ($this->areAjaxLinksEnabled()) {
      $value['#attached']['library'][] = 'core/drupal.ajax';
    }

    // @todo CSRF tokens are based on the path, user session, and a private key.
    //   The default cacheability metadata doesn't seem to work, so we're
    //   setting this to be uncacheable; but there is probably a better fix.
    $value['#cache']['max-age'] = 0;

    return $value;
  }

  /**
   * Generate an unban URL for an IP address.
   *
   * While it is recommended to check whether
   * \Drupal\dblog_ban\Services\IpValidator::isMyIp() === FALSE before
   * displaying a ban link, the consequences of the current user unbanning
   * themselves is less serious: you don't necessarily need to check that before
   * calling this function.
   *
   * @param string $ip
   *   The IP address to generate the URL for.
   *
   * @return \Drupal\Core\Url
   *   A URL for an unban link.
   */
  public function getUnbanUrl(string $ip): Url {
    $urlIp = \urlencode($ip);
    $urlOptions = $this->getUrlOptions($ip, 'unban');

    return Url::fromRoute('dblog_ban.unban', [
      'js' => 'nojs',
      'ip' => $urlIp,
    ], $urlOptions);
  }

  /**
   * Get a set of URL options for a ban/unban URL.
   *
   * @param string $ip
   *   The IP address that we're generating a ban/unban URL for.
   * @param string $action
   *   The action to take, one of 'ban' or 'unban'.
   *
   * @return array
   *   An associative array of additional URL options. See
   *   \Drupal\Core\Url::fromUri() for the format.
   */
  protected function getUrlOptions(string $ip, string $action): array {
    $destinationQueryParam = $this->getDestinationArray();
    $htmlAttributes = [];

    // If AJAX links are enabled, then add the 'use-ajax' class to trigger the
    // core/drupal.ajax library to modify the URL.
    if ($this->areAjaxLinksEnabled()) {
      $htmlAttributes['class'] = ['use-ajax'];
    }

    // Also include some information in the HTML 'data-' attributes so that
    // \Drupal\dblog_ban\Controller\IpBanUnbanAjaxController can replace the
    // link we're generating now with a new link to perform its converse
    // action; and so automated tests can click the links.
    $htmlAttributes['data-dblog_ban-ip'] = $ip;
    $htmlAttributes['data-dblog_ban-action'] = $action;

    return [
      'query' => $destinationQueryParam,
      'attributes' => $htmlAttributes,
    ];
  }

  /**
   * Determine if AJAX links are enabled in the module configuration.
   *
   * @return bool
   *   TRUE if AJAX links are enabled; FALSE otherwise.
   */
  private function areAjaxLinksEnabled(): bool {
    return $this->configFactory->get('dblog_ban.settings')
      ->get('use_ajax_links');
  }

}
