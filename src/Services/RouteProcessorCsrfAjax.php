<?php

namespace Drupal\dblog_ban\Services;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Access\RouteProcessorCsrf;
use Drupal\Core\Render\BubbleableMetadata;
use Symfony\Component\Routing\Route;

/**
 * Processes outbound ajax/nojs route to handle the CSRF token.
 *
 * @todo Remove this class when https://www.drupal.org/node/2670798 is merged.
 * @internal
 */
class RouteProcessorCsrfAjax extends RouteProcessorCsrf {

  /**
   * {@inheritdoc}
   */
  public function processOutbound($route_name, Route $route, array &$parameters, ?BubbleableMetadata $bubbleable_metadata = NULL) {
    if ($route->hasRequirement('_dblog_ban_csrf_ajax_token')) {
      $path = static::preparePath($route, $parameters);
      // Adding this to the parameters means it will get merged into the query
      // string when the route is compiled.
      if (!$bubbleable_metadata) {
        $parameters['token'] = $this->csrfToken->get($path);
      }
      else {
        // Generate a placeholder and a render array to replace it.
        $placeholder = Crypt::hashBase64($path);
        $placeholder_render_array = [
          '#lazy_builder' => [
            'dblog_ban_route_processor_csrf_ajax:renderPlaceholderCsrfToken',
            [$path],
          ],
        ];

        // Instead of setting an actual CSRF token as the query string, we set
        // the placeholder, which will be replaced at the very last moment. This
        // ensures links with CSRF tokens don't break cacheability.
        $parameters['token'] = $placeholder;
        $bubbleable_metadata->addAttachments(['placeholders' => [$placeholder => $placeholder_render_array]]);
      }
    }
  }

  /**
   * Prepares the route's path for generating a token.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The outbound route to process.
   * @param array $parameters
   *   An array of parameters to be passed to the route compiler. Passed by
   *   reference.
   *
   * @return string
   *   The route's path with parameters replaced, except for the 'js' parameter.
   */
  public static function preparePath(Route $route, array &$parameters) {
    $replacements = $parameters;
    if (isset($replacements['js']) && in_array($replacements['js'], [
      'ajax',
      'nojs',
    ])) {
      unset($replacements['js']);
    }
    $keys = array_map(
      function ($key) {
        return "{{$key}}";
      },
      array_keys($replacements)
    );

    // Replace the path parameters with values from the parameters array.
    $path = ltrim($route->getPath(), '/');
    $path = str_replace($keys, $replacements, $path);

    return $path;
  }

}
