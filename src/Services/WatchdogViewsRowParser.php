<?php

namespace Drupal\dblog_ban\Services;

use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Service to get a hostname (IP address) from a Views row of type 'watchdog'.
 */
class WatchdogViewsRowParser implements ContainerInjectionInterface {

  /**
   * A connection to Drupal's database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $connection;

  /**
   * Constructs a WatchdogViewsRowParser.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   A connection to Drupal's database.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self($container->get('database'));
  }

  /**
   * Gets a hostname (IP address) from a Views ResultRow of type 'watchdog'.
   *
   * @param \Drupal\views\ResultRow $row
   *   The Views ResultRow to parse.
   *
   * @return string|null
   *   The hostname (IP address) associated with the given row, or NULL if we
   *   can't figure out how to get it.
   */
  public function getHostnameFromWatchdogResultRow(ResultRow $row): ?string {
    // If we can find a field named 'watchdog_hostname' in the row, and it looks
    // like a hostname, return it.
    if (property_exists($row, 'watchdog_hostname') && is_string($row->watchdog_hostname)) {
      return $row->watchdog_hostname;
    }

    // If we can't find a watchdog_hostname property, but we can find a property
    // for a log entry ID, use that log entry ID to look up the hostname.
    if (property_exists($row, 'wid')) {
      return $this->getHostnameFromWid((int) $row->wid);
    }

    // If we get here, then we don't know how to get the hostname or the wid
    // from the row, so return NULL.
    return NULL;
  }

  /**
   * Get a hostname (IP address) from a database log entry with a given ID.
   *
   * @param int $wid
   *   The ID of the log entry.
   *
   * @return string|null
   *   The hostname (IP address) for the given $wid. Note some log entries
   *   (e.g.: cron runs) don't have a hostname associated with them, so this
   *   will return NULL in that case. This function will also return NULL if
   *   a log entry with the given $wid cannot be found.
   */
  public function getHostnameFromWid(int $wid): ?string {
    // Speed is essential here, so we try to use a simple query static query
    // that Drupal won't need to compile, and that the database can easily
    // optimize.
    $hostname = $this->connection
      ->query("SELECT hostname FROM {watchdog} WHERE [wid] = :wid LIMIT 1", [
        ':wid' => $wid,
      ])
      ->fetchField();

    // If the query indicates we didn't find any matching log entries, then
    // return NULL, i.e.: as if the hostname wasn't set for that row.
    if ($hostname === FALSE) {
      return NULL;
    }

    return $hostname;
  }

}
