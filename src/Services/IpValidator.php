<?php

namespace Drupal\dblog_ban\Services;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Service to validate hostnames or IP addresses.
 */
class IpValidator implements ContainerInjectionInterface {

  /**
   * Stack of HTTP requests and sub-requests that resulted in this code running.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * Constructs a HostnameValidator.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Stack of HTTP requests and sub-requests that resulted in this code
   *   running.
   */
  public function __construct(RequestStack $requestStack) {
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self($container->get('request_stack'));
  }

  /**
   * Return the IP used in the current request.
   *
   * @return string|null
   *   The IP for the current request or NULL if one cannot be found.
   */
  public function getCurrentRequestIp(): ?string {
    $currentRequest = $this->requestStack->getCurrentRequest();
    if ($currentRequest instanceof Request) {
      return $currentRequest->getClientIp();
    }
    return NULL;
  }

  /**
   * Return TRUE if the IP address is valid, FALSE otherwise.
   *
   * This check is useful to avoid errors when banning or unbanning an IP
   * address.
   *
   * @param string|null $hostname
   *   The hostname or IP address to validate, i.e.: from a row in the dblog
   *   module's watchdog table.
   *
   * @return bool
   *   TRUE if the given hostname is valid to ban or unban; FALSE otherwise.
   */
  public function isValidIp(?string $hostname): bool {
    // As per \ban_schema(), the IP address is not allowed to be NULL.
    if (is_null($hostname)) {
      return FALSE;
    }

    // As per \ban_schema(), the IP address cannot be longer than 40 ASCII
    // characters (i.e.: bytes) long, so we're using \strlen() here instead of
    // something like \mb_strlen().
    if (\strlen($hostname) > 40) {
      return FALSE;
    }

    // Use PHP's builtin \filter_var() to ensure the hostname is valid and not
    // in a private or reserved IP address range. See
    // https://www.php.net/manual/function.filter-var.php and
    // https://www.php.net/manual/filter.filters.validate.php for more
    // information.
    return (\filter_var($hostname, FILTER_VALIDATE_IP, FILTER_FLAG_NO_RES_RANGE) !== FALSE);
  }

  /**
   * Return TRUE if the given hostname is the IP used in the current request.
   *
   * This check is useful to avoid letting a user ban themselves.
   *
   * @param string|null $hostname
   *   The hostname or IP address to compare with the current request, i.e.:
   *   from a row in the dblog module's watchdog table.
   *
   * @return bool
   *   TRUE if the given hostname is the same as the IP address from the current
   *   request.
   */
  public function isMyIp(?string $hostname): bool {
    return $hostname === $this->getCurrentRequestIp();
  }

}
