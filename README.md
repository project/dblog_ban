# Database logging ban operation

As an administrator, I want to be able to ban misbehaving IP addresses from the list of database logs, so that I can curb spammers on my site.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/dblog_ban).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/dblog_ban).


## Table of contents (optional)

- Requirements
- Installation
- Configuration
- Troubleshooting & FAQ


## Requirements

This module requires the following modules:

- The Ban module (in Drupal core)
- The Database Logging module (in Drupal core)


## Installation

1. Install [as you would normally install a contributed Drupal
    module](https://www.drupal.org/docs/user_guide/en/extend-module-install.html).


## Configuration

1. Edit the watchdog view at `/admin/structure/views/view/watchdog`:
    1. Add a new field named "Ban/Unban link" (machine name
        `dblog_ban_ban_unban_link`) to the view.
    2. Save the modified view.
2. Visit the watchdog view at `/admin/reports/dblog` to see the Ban/Unban link
    field.


## Troubleshooting & FAQ

The Database logging ban operation module *will not show a ban/unban link* if:

1. The 'hostname' part of the log message does not validate as an IP address
    (using the same validation function performed when you try to ban an IP
    address through the Drupal UI), and/or,
2. The 'hostname' part of the log message matches the IP address of the current
    page request.

As with Drupal's built-in IP-address-banning functionality, this module tries to
prevent you from banning your own IP accidentally, but if you access the site
from multiple IP addresses (e.g.: from two different locations, or from one
location that uses a networking bridge, or certain large networks such as those
at a university), Drupal and this module are unable to recognize that, and so it
is still (at least theoretically) possible to accidentally ban yourself. Thus,
as with Drupal core's IP address banning functionality, please exercise caution.
